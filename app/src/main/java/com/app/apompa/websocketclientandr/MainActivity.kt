package com.app.apompa.websocketclientandr

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import okhttp3.*
import okio.ByteString


class MainActivity : AppCompatActivity() {
    private var start: Button? = null
    private var stop: Button? = null
    private var output: TextView? = null
    private var client: OkHttpClient? = null
    private var ws: WebSocket? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        start = findViewById(R.id.start)
        stop = findViewById(R.id.stop)
        output = findViewById(R.id.output)
        client = OkHttpClient()

        start!!.setOnClickListener { startWs() }
        stop!!.setOnClickListener { stopWs() }

    }

    private fun startWs() {
        //val request: Request = Request.Builder().url("wss://demos.kaazing.com/echo").build()
        val request: Request = Request.Builder().url("wss://echo.websocket.org").build()
        //val request: Request = Request.Builder().url("wss://localhost:8080/myws/echo").build()
        val listener = EchoWebSocketListener()
        ws = client!!.newWebSocket(request, listener)
    }

    fun stopWs() {
        //client!!.dispatcher().executorService().shutdown()
        client!!.dispatcher().cancelAll()
        ws!!.close(NORMAL_CLOSURE_STATUS, "Goodbye !")
    }

    private fun output(txt: String) {
        runOnUiThread { output!!.text = output!!.text.toString() + "\n\n" + txt }
    }


    inner class EchoWebSocketListener : WebSocketListener() {
        override fun onOpen(webSocket: WebSocket, response: Response?) {
            webSocket.send("Hello, it's Adrian !")
            webSocket.send("What's up ?")
            webSocket.send("Testing client ws Android")
            webSocket.send(ByteString.decodeHex("deadbeef"))
        }

        override fun onMessage(webSocket: WebSocket, text: String) {
            output("Receiving : $text")
        }

        override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
            output("Receiving bytes : " + bytes.hex())
        }

        override fun onClosing(
            webSocket: WebSocket,
            code: Int,
            reason: String
        ) {
            webSocket.close(NORMAL_CLOSURE_STATUS, null)
            output("Closing : $code / $reason")
        }

        override fun onFailure(
            webSocket: WebSocket?,
            t: Throwable,
            response: Response?
        ) {
            output("Error : " + t.message)
            webSocket!!.close(NORMAL_CLOSURE_STATUS, null)
        }

        override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
            output("onClosed : $code / $reason")
        }
    }

    companion object {
        private const val NORMAL_CLOSURE_STATUS = 1000
    }
}